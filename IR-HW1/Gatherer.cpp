
#include "Gatherer.h"
#include <algorithm>    // std::remove_if

bool is_non_alphabet(char c) {
	if ((c <= 'z' && c >= 'a') || (c <= 'Z' && c >= 'A'))
		return false;
	else
		return true;
};

Gatherer::Gatherer(DataManager* dm)
{
	this->dm = dm;
}


Gatherer::~Gatherer()
{
}


void Gatherer::readFile(int docID) {
	std::ifstream fin;
	std::string str;
	std::string fileName;
	fileName = dm->file_list[docID];
	fin.open("docs/"+ fileName+".txt");
	while (fin >> str) {
		str.erase(std::remove_if(str.begin(), str.end(), is_non_alphabet), str.end());
		dm->addWord(str, docID);
	}
}

std::string Gatherer::cleanString(std::string word) {
	word.erase(std::remove_if(word.begin(), word.end(), is_non_alphabet), word.end());
	return word;
}

