/*
Thai Thien
1351040
IR-HW1
*/
#include "DataManager.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include "Gatherer.h"
void DataManager::addWord(std::string word, int docID) {
	Entry* m_entry = NULL;
	m_entry = findWord(word);
	if (m_entry == NULL) {
		m_entry = new Entry(word, docID);
		wordList.push_front(m_entry);
	}
	else
	{
		m_entry->addDocID(docID);
	}
}

Entry* DataManager::findWord(std::string word) {
	Entry* result = NULL;
	for each (Entry* w in wordList) {
		if (w->getWord() == word) {
			result = w;
			return result;
		}
	}
	return result;
}

void DataManager::printDictionary() {
	std::ofstream fout;
	fout.open("Dictionary.txt");
	for each (Entry* w in wordList) {
		//std::cout << w->getWord() << "\n";
		fout << w->getWord() << "\n";
	}
}

void DataManager::printInvertedIndex() {
	for each (Entry* w in wordList) {
		std::cout << w->toString() << "\n";
	}
}

DataManager::DataManager()
{
}


DataManager::~DataManager()
{
}



Entry* DataManager::intersection(Entry* a, Entry* b) {
	if (a == NULL || b == NULL)
		return NULL;
	Node* cur_a = a->getHead();
	Node* cur_b = b->getHead();
	Entry* result = new Entry("", -1);// init an empty entry for holding the intersection
	while (cur_a != NULL && cur_b != NULL) {
		if (cur_a->ID == cur_b->ID)
		{
			result->addDocID(cur_a->ID);
			cur_a = cur_a->pNext;
			cur_b = cur_b->pNext;
			continue;
		}

		if (cur_a->ID > cur_b->ID) {
			// a > b => increase b
			cur_b = cur_b->pNext;
		}
		else
		{
			//b > a => increase a
			cur_a = cur_a->pNext;
		}	
	}
	return result;
}


Entry* DataManager::searchQuery(std::string query) {
	
	using namespace std;
	istringstream iss(query);
	vector<string> word_tk;
	copy(istream_iterator<string>(iss),
		istream_iterator<string>(),
		back_inserter(word_tk));
	Entry* result = NULL;
	if (word_tk.size() == 0)
		return result;
	
	Entry *one = findWord(Gatherer::cleanString(word_tk[0]));
	Entry *two = NULL;
	for (int i = 1; i < word_tk.size(); i++) {
		two = findWord(Gatherer::cleanString(word_tk[i]));
		one = intersection(one, two);
	}

	return one;
}