/*
	Thai Thien
	1351040
	IR-HW1
*/
#include <string>
#include <fstream>
#include <streambuf>
#include <iostream>
#include <sstream>

#include "Gatherer.h"
#include "Entry.h"
#include "DataManager.h"

void testEntry();
void testGatherer();
void testEntry();
void testDataManager();

int main() {
	std::ifstream fin;
	std::string str;
	std::ofstream fout;
	
	fout << "meow";


	
	DataManager dm;
	Gatherer gather(&dm);
	std::cout << "Start to read from file \n";
	for (int i = 0; i < dm.file_list.size(); i++) {
		gather.readFile(i);// Read text file
	}
	std::cout << "Start to print into dictionary.txt \n";
	
	//a
	dm.printDictionary();// print dictionary to text file

	//b
	std::string a_term;
	std::cout << "b) Building Inverted Index: list all documents that contain a given term (input a term) \n";
	std::cin >> a_term;
	a_term = Gatherer::cleanString(a_term);
	Entry* a_term_result;
	a_term_result = dm.findWord(a_term);
	if (a_term_result != NULL) {
		std::cout << a_term_result->toString() << "\n";
	}
	else {
		std::cout << "Not found "<< a_term<<"\n";
	}
	

	//c 
	std::string tmp_aaa;
	std::string query;
	std::cout << "Enter the string for query \n";
	std::getline(std::cin, tmp_aaa);//it is require for next readline
	std::getline(std::cin, query);// read a line of string

	
	
	Entry* queryResult = dm.searchQuery(query);

	if (queryResult != NULL) {
		std::cout << "Document that contain all the term in query string is ";
		std::cout << queryResult->toString() << "\n";
	}
	else
	{
		std::cout << "No document contain all term in query string\n";
	}
}


void testGatherer() {
	DataManager dm;
	Gatherer gather(&dm);
	gather.readFile(1);
	dm.printDictionary();
}

void testEntry() {
	Entry entry("dog", 3);
	entry.addDocID(1);
	entry.addDocID(9);
	entry.addDocID(5);
	entry.addDocID(10);
	entry.addDocID(11);
	entry.addDocID(9);
	entry.addDocID(1);
	std::cout << entry.toString()<<"\n";
	std::cout << entry.searchDocID(3) << " " << entry.searchDocID(4)<<"\n";
}

void testDataManager() {
	DataManager dm;
	dm.addWord("breakthrough", 1);
	dm.addWord("drug", 1);
	dm.addWord("for", 1);
	dm.addWord("diabetes", 1);

	dm.addWord("new", 2);
	dm.addWord("diabetes", 2);
	dm.addWord("drug", 2);

	dm.addWord("new", 3);
	dm.addWord("approach", 3);
	dm.addWord("for", 3);
	dm.addWord("treatment", 3);
	dm.addWord("of", 3);
	dm.addWord("diabetes", 3);

	dm.addWord("new", 4);
	dm.addWord("hopes", 4);
	dm.addWord("for", 4);
	dm.addWord("diabetes", 4);
	dm.addWord("patients", 4);
	
	std::cout << "DICTIONARY " << "\n";
	dm.printDictionary();
	std::cout << "Inverted Index" << "\n";
	dm.printInvertedIndex();
}