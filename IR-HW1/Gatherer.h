/*
Thai Thien
1351040
IR-HW1
*/
#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include "DataManager.h"
class Gatherer
{
private: 
	DataManager* dm;
public:
	Gatherer(DataManager* dm);
	~Gatherer();
	
	//read from file name file_list[docID]
	void readFile(int docID);
	static std::string cleanString(std::string word);
};

