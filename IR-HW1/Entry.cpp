/*
Thai Thien
1351040
IR-HW1
*/
#include "Entry.h"



Entry::Entry(std::string word, int docID)
{
	this->word = word;
	if (docID == -1) {
		this->head == NULL;
	}
	else {
		this->head = new Node();
		this->head->ID = docID;
		this->head->pNext = NULL;
	}
}

bool Entry::searchDocID(int docID) {
	Node * cur;
	cur = this->head;
	while (cur != NULL)
	{
		if (cur->ID == docID) {
			return true;
		}
		else {
			cur = cur->pNext;
		}
	}

	return false;
}

void Entry::addDocID(int docID) {
	Node * cur;
	Node * last = NULL;
	cur = this->head;

	if (this->head == NULL)
	{
		// when head is null
		this->head = new Node();
		this->head->ID = docID;
		this->head->pNext = NULL;
		return;
	}

	while (cur != NULL)
	{
		if (cur->ID == docID) {
			return;
		}

		if (cur->ID > docID) {
			// cur < docID, and last not null, insert into last
			if (last != NULL) {
				last->pNext = new Node();
				last->pNext->ID = docID;
				last->pNext->pNext = cur;
				return;

			}
			else
			{
				// insert at start of list
				head = new Node();
				head->ID = docID;
				head->pNext = cur;
				return;
			}
		}

		// move on
		last = cur;
		cur = cur->pNext;
	}

	// docID is largest, insert at the end of list
	last->pNext = new Node();
	last->pNext->ID = docID;
	last->pNext->pNext = cur;

}

std::string Entry::toString() {
	
	std::string str = "";
	str += this->word + " ";
	Node* cur = head;
	while (cur != NULL) {
		str += "__"+ std::to_string(cur->ID);
		cur = cur->pNext;
	}
	return str;
}

std::string Entry::getWord() {
	return word;
}

Node* Entry::getHead() {
	return this->head;
}

Entry::~Entry()
{
}
