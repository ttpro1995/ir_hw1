/*
Thai Thien
1351040
IR-HW1
*/
#pragma once
#include <list>
#include "Entry.h"
#include <vector>
class DataManager
{
private:
	std::list<Entry*> wordList;// contain all the word and its inverted index
public:

	// all file name
	std::vector<std::string> file_list = 
	{ "A Festival of Books"
		, "A Murder-Suicide"
	,"Better To Be Unlucky"
	,"Cloning Pets"
	,"Crazy Housing Prices"
	,"Food Fight Erupted in Prison"
	,"Freeway Chase Ends at Newsstand"
	,"Gasoline Prices Hit Record High"
	,"Happy and Unhappy Renters"
	,"Jerry Decided To Buy a Gun"
	,"Man Injured at Fast Food Place"
	,"Pulling Out Nine Tons of Trash"
	,"Rentals at the Oceanside Community"
	,"Sara Went Shopping"
	,"Trees Are a Threat"};
	
	DataManager();
	void addWord(std::string word, int docID);// add word into word list
	Entry* findWord(std::string word);// find an Entry of word in word list
	void printDictionary();// print the dictionary
	void printInvertedIndex();// print the inverted list
	~DataManager();

	static Entry* intersection(Entry* a, Entry* b);//do intersection between 2 entry, return an entry contain docID with belong to both a,b
	Entry* searchQuery(std::string query);//search a document that have all term in the query
};

