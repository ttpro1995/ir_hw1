/*
Thai Thien
1351040
IR-HW1
*/
#pragma once
#include <string>
struct Node
{
	Node* pNext;
	int ID;
};


class Entry
{
private:
	std::string word;// a word
	Node* head;// a linked list, each node is a docID of document contain that word

public:
	
	Entry(std::string word, int docID);
	~Entry();

	bool searchDocID(int docID);// search if docID is in the linked list

	void addDocID(int docID);// add docID to the linked list in increasing order
	std::string getWord();//get word
	std::string toString();// get string consists of a word and inverted index 
	Node* getHead();

};

